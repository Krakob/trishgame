## Trish's Wacko Adventure
... is a free and open source Game Boy-like platformer game developed in the Godot Engine. The game is currently under development.
Follow the devlog at the [trishgame Tumblr](http://trishgame.tumblr.com)!

### Software used
* **Game engine**: [Godot 3.0](https://godotengine.org)
* **Audio**: [LMMS 1.2-rc5](https://lmms.io)
* **Graphics**: [Krita 3.3.3](https://krita.org)
* **Programming**: [Visual Studio Code](https://code.visualstudio.com/) and [Neovim](https://neovim.io/) (anything will do!)

The project may migrate to later stable versions of these programs as they release, especially Godot 3.0, which has a decent bit of bug-fixing to do.

### File conventions
All files part of the game itself should be in the `/src` directory. Any subcategories of resources are children of this directory. Directories may not be further nested, other than `_src` directories, which are used for storing files not directly used by the Godot Engine, such as Krita drawing documents and LMMS audio documents. Spaces in filenames should be avoided.

### Graphics conventions
The game uses a greyscale palette with the colours (for all values of RGB) 64, 128, 192, and 255. Modification of these colours is handled via shaders.
Sprites should consist of only three of these colours as well as transparency. Background tiles may use all colours.

### Audio conventions
Audio consists solely of sounds produced by the LMMS FreeBoy instrument. Sound effects should utilise only one channel. Music should only use one voice per channel, although the two square wave channels may be combined into one. Likewise, the wave channel may remain unused in exchange for another square voice, as the wave channel can be used to emulate a square wave anyhow.  Drums may overlap with other channels, although they mostly utilise the noise channel.

### Deviations from Game Boy limitations
Some of the issues of the Game Boy have been ignored, both for convenience of development and because these limitations would be challenging and time consuming to enforce.

* Sprites and tiles were limited to 8x8, but they would be combined to form greater objects.
* There are no limits on amount of rendered sprites.
* Sound channels aren't perfectly monophonic due to overlap with drums and sound effects.

### License
All files with the `.gd` extension are available under the MIT license, see `LICENSE-CODE.md`.

All other files are available under the CC-0 license, see `LICENSE-ASSETS.md`.

In layman's terms, it means you can basically do whatever you like with the project files. Attribution is encouraged, but not required :)

### Credits

**Game developer**: Jakob Schramm

**Tool developers**: Juan Linietsky, Ariel Manzur, and all other Godot contributors; the LMMS team; the KDE team

**Rubber ~~duck~~ bear QA**: [Zeddy](https://github.com/zeddidragon)

**Main character**: [Trish](http://psmendiola.com/)