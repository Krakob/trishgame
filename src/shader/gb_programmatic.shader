shader_type canvas_item;

uniform float subtractive : hint_range(-2.0, 2.0);
uniform float plain_multiplier : hint_range(-2.0, 2.0);
uniform vec4 color_multiplier : hint_color;

void fragment () {
    vec4 grid_pixel_color = texture(TEXTURE, UV);
    COLOR = (grid_pixel_color - vec4(vec3(subtractive), 0.0)) * plain_multiplier * color_multiplier;
}