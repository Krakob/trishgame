extends RigidBody2D

func _on_projectile_body_enter( body ):
	if body.has_method("hit_by_projectile"):
		body.call("hit_by_projectile")
		$anim.play("explode")

func _on_Timer_timeout():
	$anim.play("explode")


func _on_anim_animation_finished( anim_name ):
	queue_free()
